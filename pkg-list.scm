;; Copyright (C) 2010, 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

;;; Commentary:

;;; Code:

(package (wak-foof-loop (0) (2010 4 27) (1))
  (depends (srfi-1)
           (srfi-8)
           (srfi-45)
           (wak-common)
           (wak-syn-param)
           (wak-riastreams))
  (synopsis "extensible looping library")
  (description
   "foof-loop allows for convenient expression of loops."
   "It contains built-in iterators for numeric ranges, lists,"
   "vectors and ports."
   "It is extensible by adding new iterators, for example for traversing"
   "other data structures.")
  (homepage "http://home.gna.org/wak/")
  (libraries
   (sls -> "wak")
   (("foof-loop" "private") -> ("wak" "foof-loop" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
