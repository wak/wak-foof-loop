;;; foof-loop.sps --- R6RS wrapper for the foof-loop test suite

;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

;;; Commentary:

;;; Code:
#!r6rs

(import (rnrs)
        (rnrs r5rs)
        (rnrs mutable-strings)
        (wak private include)
        (wak trc-testing)
        (wak foof-loop))

(include-file ((wak foof-loop private) test-foof-loop))
